#!/bin/bash
# author:xsj
read -r -p $'==========此脚本将使用当前目录下的docker-compose.yaml 构建fastdfs5.11的docker镜像==========\n==========请确认docker-compose.yaml 文件中各服务端口号、IP、数据映射目录是否已配置==========\n==========是否继续构建? [Y/N]========== ' input
case $input in
    [yY][eE][sS]|[yY])
		echo "-----开始构建fastdfs5.11 的docker镜像-----"
		sudo docker-compose up -d --build
		echo "-----fastdfs5.11 的docker镜像 构建成功-----"
		docker ps -a |grep fastdfs
		;;

    [nN][oO]|[nN])
		echo "未做操作，退出构建"
       	;;

    *)
		echo "错误的输入，退出构建"
		exit 1
		;;
esac
